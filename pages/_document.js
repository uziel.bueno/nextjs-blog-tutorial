import Document, { Html, Head, Main, NextScript } from 'next/document';

export default class SiteDocument extends Document {
    static async getInitialProps(ctx) {
        const initialProps = await Document.getInitialProps(ctx);
        return { ...initialProps };
    }

    render() {
        return (
            <Html lang="en">
                <Head />
                <body className="bg-gray-100 antialiased font-sans">
                    <Main />
                    <NextScript />
                </body>
            </Html>
        );
    }
}

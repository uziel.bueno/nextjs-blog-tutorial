import Head from 'next/head';
import Layout from '../../components/layout';
import Date from '../../components/date';
import { getAllPostsIds, getPostData } from '../../lib/posts';
import style from './post.module.css';
import utilStyles from '../../styles/utils.module.css';

export default function Post({ postData }) {
    return (
        <Layout>
            <Head>
                <title>{postData.title}</title>
            </Head>
            <article>
                <h2 className="text-3xl font-bold leading-tight">{postData.title}</h2>
                <p className="mt-4 text-gray-800">
                    <Date dateString={postData.date} />
                </p>
                <div className={`${style['markdown']} mt-4 leading-7 text-lg`} dangerouslySetInnerHTML={{ __html: postData.contentHtml }} />
            </article>
        </Layout>
    );
}

export async function getStaticPaths() {
    const paths = getAllPostsIds();
    return {
        paths,
        fallback: false
    };
}

export async function getStaticProps({ params }) {
    const postData = await getPostData(params.id);
    return {
        props: {
            postData
        }
    };
}
